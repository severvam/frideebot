# Fridee Bot for Telegram
Telegram bot implementation for Autokadabra.ru "Пятниццо" posts

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Java 11+

MySQL 8+


### Installing

Install MySQL database. Update "initial.sql" script, change password "secret" to your password. Run this inital script to create database.

Build project using Gradle

`./gradlew shadowJar`

Copy runbot.sh and frideebot-fat.jar file to bot home directory. frideebot-fat.jar located at %PROJECT_SOURCE_HOME%/build/libs/frideboot-fat.jar 

In **runbot.sh** set environment variables:

TELEGRAM_TOKEN= your token

TELEGRAM_DB_PASS= db-password

TEST_TELEGRAM_CHAT= telegram-chat-id

Run `./runbot.sh &` in terminal.

Have fun!

## Intellij IDEA setup

* Enable annotation processing
* Set gradle output directory as 'Module content root' for annotation processing like 'build/generated/sources/annotationProcessor/java/main' for main codeline and 'build/generated/sources/annotationProcessor/java/test' for test 

##Built With

* [vert.x](https://vertx.io/) - Reactive app toolkit
* [Google Dagger 2](https://google.github.io/dagger/) - Lightweight dependency injection framework
* [Dieselpoint Norm](https://github.com/dieselpoint/norm) - Norm is a simple way to access a JDBC database, usually in one line of code. It purges your code of the complex mess that is Hibernate, JPA, and ORM.
* [Gradle](https://gradle.org/) - Build and Dependency management tool


## Versioning

We use XX.YY.ZZ for versioning. Where

* XX - last two digits from the year. For example 2018 -> 18
* YY - Month. Using leading zero for one digit month. For example September -> 09
* ZZ - Current month release sequence number. Starts from zero. Reset every month. For example 5th build for this month -> 4

So, 5th released version for September 2018 is: 18.09.4

## Authors
 
* **Alexander Severgin** - *Initial work* - [severvam](https://gitlab.com/severvam)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details