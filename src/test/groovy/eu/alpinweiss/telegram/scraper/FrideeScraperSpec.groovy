package eu.alpinweiss.telegram.scraper

import spock.lang.Specification

class FrideeScraperSpec extends Specification {

    def frideeScraper = new FrideeScraper()

    void 'should scrape post list'() {
        when:
        def fridees = frideeScraper.scrapePosts('http://autokadabra.ru/users/J1mm/shouts')
        then:
        fridees.size() > 0
    }


    void 'should scrape particular post'() {
        when:
        def fridee = frideeScraper.scrape('http://autokadabra.ru/shouts/' + 104743)
        then:
        fridee.title == 'Пятниццо #403'
        fridee.photos.size() == 50
        fridee.videos.size() == 1
    }

    void 'should scrape particular post with different format'() {
        when:
        def fridee = frideeScraper.scrape('http://autokadabra.ru/shouts/' + 104789)
        then:
        fridee.title == 'Пятниццо #404'
        fridee.photos.size() == 50
        fridee.videos.size() == 1
    }
}
