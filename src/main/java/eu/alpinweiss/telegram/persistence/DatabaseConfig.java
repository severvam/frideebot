package eu.alpinweiss.telegram.persistence;

import com.dieselpoint.norm.Database;

class DatabaseConfig {

	private final Database db;

	private static DatabaseConfig dbConfigInstance;

	private DatabaseConfig(String url,
	                       String port,
	                       String databaseName,
	                       String user) {
		db = new Database();
		db.setJdbcUrl("jdbc:mysql://" + url + ":" + port + "/" + databaseName + "?parseTime=true&useSSL=false&allowPublicKeyRetrieval=true&useUnicode=yes&characterEncoding=UTF-8");
		db.setUser(user);
		db.setPassword(System.getenv("TELEGRAM_DB_PASS"));
	}

	Database db() {
		return db;
	}

	static DatabaseConfig getInstance(String url, String port, String databaseName, String user) {
		if (dbConfigInstance == null) {
			dbConfigInstance = new DatabaseConfig(url, port, databaseName, user);
		}
		return dbConfigInstance;
	}

}
