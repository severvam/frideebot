package eu.alpinweiss.telegram.persistence.fridee;

import eu.alpinweiss.telegram.persistence.Persistable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fridee")
public class Fridee implements Persistable<Integer> {

	private Integer id;
	private String title;
	private String url;
	private Date createdDate;
	private Date publishedDate;
	private Boolean enabled;

	@Id
	@GeneratedValue
	@Override
	public Integer getId() {
		return id;
	}

	public static Fridee init(String title, String url, Date date) {
		return new Fridee(null, title, url, new Date(), date, true);
	}

}
