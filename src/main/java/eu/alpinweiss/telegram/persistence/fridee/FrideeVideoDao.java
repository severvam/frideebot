package eu.alpinweiss.telegram.persistence.fridee;

import com.dieselpoint.norm.Transaction;
import eu.alpinweiss.telegram.persistence.AbstractDao;
import eu.alpinweiss.telegram.persistence.DatabaseConfigInitializer;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class FrideeVideoDao extends AbstractDao<FrideeVideo, Integer> {

	@Inject
	public FrideeVideoDao(DatabaseConfigInitializer databaseConfigInitializer) {
		super(databaseConfigInitializer);
	}

	@Override
	public Class<FrideeVideo> getEntityClass() {
		return FrideeVideo.class;
	}

	public void saveAllInTransaction(List<FrideeVideo> frideeVideos, Transaction transaction) {
		for (FrideeVideo frideeVideo : frideeVideos) {
			database().transaction(transaction).insert(frideeVideo);
		}
	}

	public List<FrideeVideo> readLatest(int frideeId) {
		return database().sql("SELECT v.* FROM fridee_video v JOIN fridee f on f.id = v.fridee_id where v.enabled = true and v.posted = false and f.enabled = true and f.id = ?", frideeId).results(getEntityClass());
	}

	public FrideeVideo randomVideo() {
		final var videoId = database().sql("SELECT f.id FROM fridee_video f JOIN (SELECT RAND() * (SELECT MAX(id) FROM fridee_video) AS max_id) AS m WHERE f.id >= m.max_id ORDER BY f.id ASC LIMIT 1")
				.results(Integer.class).stream().findFirst().orElseThrow();
		return find(videoId).orElseThrow();
	}

}
