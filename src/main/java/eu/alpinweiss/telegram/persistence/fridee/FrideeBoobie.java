package eu.alpinweiss.telegram.persistence.fridee;

import eu.alpinweiss.telegram.persistence.Persistable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fridee_boobie")
public class FrideeBoobie implements Persistable<Integer> {

	private Integer id;
	private Integer frideeId;
	private String url;
	private Boolean enabled;
	private Boolean posted;

	@Id
	@GeneratedValue
	@Override
	public Integer getId() {
		return id;
	}

	@Column(name = "fridee_id")
	public Integer getFrideeId() {
		return frideeId;
	}

	public static FrideeBoobie init(Integer frideeId, String url) {
		return new FrideeBoobie(null, frideeId, url, true, false);
	}

}
