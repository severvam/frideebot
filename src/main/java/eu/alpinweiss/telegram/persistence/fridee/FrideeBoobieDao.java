package eu.alpinweiss.telegram.persistence.fridee;

import com.dieselpoint.norm.Transaction;
import eu.alpinweiss.telegram.persistence.AbstractDao;
import eu.alpinweiss.telegram.persistence.DatabaseConfigInitializer;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class FrideeBoobieDao extends AbstractDao<FrideeBoobie, Integer> {

	@Inject
	public FrideeBoobieDao(DatabaseConfigInitializer databaseConfigInitializer) {
		super(databaseConfigInitializer);
	}

	@Override
	public Class<FrideeBoobie> getEntityClass() {
		return FrideeBoobie.class;
	}

	public void saveAllInTransaction(List<FrideeBoobie> boobieList, Transaction transaction) {
		for (FrideeBoobie frideeBoobie : boobieList) {
			database().transaction(transaction).insert(frideeBoobie);
		}
	}

	public List<FrideeBoobie> readLatest(int limit) {
		return database().sql("SELECT b.* FROM fridee_boobie b JOIN fridee f on f.id = b.fridee_id where b.enabled = true and b.posted = false and f.enabled = true ORDER BY f.publishedDate DESC limit ?", limit).results(getEntityClass());
	}

	public FrideeBoobie randomPhoto() {
		final var boobieId = database().sql("SELECT f.id FROM fridee_boobie f JOIN (SELECT RAND() * (SELECT MAX(id) FROM fridee_boobie) AS max_id) AS m WHERE f.id >= m.max_id ORDER BY f.id ASC LIMIT 1")
				.results(Integer.class).stream().findFirst().orElseThrow();
		return find(boobieId).orElseThrow();
	}

	public List<FrideeBoobie> findByFridee(Fridee fridee) {
		return database().where("fridee_id = ?", fridee.getId()).results(getEntityClass());
	}

}
