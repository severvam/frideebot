package eu.alpinweiss.telegram.persistence.fridee;

import com.dieselpoint.norm.Transaction;
import eu.alpinweiss.telegram.persistence.AbstractDao;
import eu.alpinweiss.telegram.persistence.DatabaseConfigInitializer;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class FrideeDao extends AbstractDao<Fridee, Integer> {

	@Inject
	public FrideeDao(DatabaseConfigInitializer databaseConfigInitializer) {
		super(databaseConfigInitializer);
	}

	@Override
	public Class<Fridee> getEntityClass() {
		return Fridee.class;
	}

	public List<Fridee> findStoredPosts(List<String> titles) {
		final var titleArray = titles.toArray();
		return database().where("title in (" + generatePlaceholders(titleArray) + ")", titleArray).results(getEntityClass());
	}

	public void saveInTransaction(Fridee fridee, Transaction transaction) {
		database().transaction(transaction).insert(fridee);
	}

	public List<String> frideeTitleList() {
		return frideeTitleList(0);
	}

	public List<String> frideeTitleList(int limit) {
		var sql = "SELECT f.title FROM fridee f WHERE f.enabled = true ORDER BY f.publishedDate DESC";
		if (limit > 0) {
			sql += " LIMIT ?";
			return database().sql(sql, limit).results(String.class);
		}
		return database().sql(sql).results(String.class);
	}

	public Fridee findByFrideeTitleNumber(String titlePart) {
		return database().where("title like ?", "%" + titlePart + "%").results(getEntityClass()).stream().findFirst().orElseThrow();
	}

}
