package eu.alpinweiss.telegram.persistence;

public interface Persistable<T> {

	T getId();

	void setId(T id);

}
