package eu.alpinweiss.telegram.persistence;

import com.dieselpoint.norm.Database;
import com.dieselpoint.norm.Transaction;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.IntStream.range;

public abstract class AbstractDao<T extends Persistable<I>, I> {

	private final DatabaseConfig databaseConfig;

	public AbstractDao(DatabaseConfigInitializer databaseConfigInitializer) {
		this.databaseConfig = databaseConfigInitializer.init();
	}

	public void persist(T entity) {
		database().insert(entity);
	}

	public Optional<T> find(T entity) {
		return find(entity.getId());
	}

	public Optional<T> find(I id) {
		final var results = database().where("id = ?", id).results(getEntityClass());
		if (!results.isEmpty()) {
			return results.stream().findFirst();
		}
		return Optional.empty();
	}

	public void update(T entity) {
		database().update(entity);
	}

	public void delete(I id) {
		database().table(getTableName()).where("id = ?", id).delete();
	}

	public void markPosted(I id) {
		database().sql("update " + getTableName() + " set posted = true where id = ?", id).execute();
	}

	public void delete(T entity) {
		database().delete(entity);
	}

	public List<T> findByIds(List<I> ids) {
		final var idArray = ids.toArray();
		return database().where("id in (" + generatePlaceholders(idArray) + ")", idArray).results(getEntityClass());
	}

	public String getTableName() {
		try {
			final var pojoInfo = database().getSqlMaker().getPojoInfo(getEntityClass());
			return String.valueOf(FieldUtils.readField(pojoInfo, "table", true));
		} catch (IllegalAccessException e) {
			//
		}
		return null;
	}

	public Transaction startTransaction() {
		return database().startTransaction();
	}

	protected Database database() {
		return databaseConfig.db();
	}

	protected String generatePlaceholders(Object[] ids) {
		final var placeholders = new ArrayList<String>();
		range(0, ids.length).forEach(index -> placeholders.add("?"));
		return String.join(",", placeholders);
	}

	public abstract Class<T> getEntityClass();

}
