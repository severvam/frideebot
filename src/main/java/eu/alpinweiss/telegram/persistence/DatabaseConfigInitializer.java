package eu.alpinweiss.telegram.persistence;

import javax.inject.Singleton;

@Singleton
public class DatabaseConfigInitializer {

	private final String url;
	private final String port;
	private final String databaseName;
	private final String user;

	public DatabaseConfigInitializer() {
		url = "localhost";
		port = "3306";
		databaseName = "frideebot";
		user = "frideebot";
	}

	DatabaseConfig init() {
		return DatabaseConfig.getInstance(this.url, this.port, this.databaseName, this.user);
	}

}
