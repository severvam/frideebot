package eu.alpinweiss.telegram;

import eu.alpinweiss.telegram.api.TelegramBotApi;
import eu.alpinweiss.telegram.chat.CommandExecutor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executors;

@Slf4j
class ChatPollingService {

	private final CommandExecutor commandExecutor;
	private final TelegramBotApi telegramApi;

	ChatPollingService(CommandExecutor commandExecutor, TelegramBotApi telegramApi) {
		this.commandExecutor = commandExecutor;
		this.telegramApi = telegramApi;
	}

	void startPolling() {
		final var executor = Executors.newSingleThreadExecutor();
		executor.submit(() -> {

			log.info("Thread for input command listening is started.");
			var offset = 1L;
			while (true) {
				final var updateResponse = telegramApi.getUpdates(offset);

				if (updateResponse.isPresent()) {
					final var response = updateResponse.get();
					for (final var update : response.getResult()) {
						final var message = update.getMessage();
						if (message == null) {
							log.info("Received empty message, skip");
							continue;
						}

						final var chat = message.getChat();

						try {
							commandExecutor.executeCommand(message.getText(), chat.getId());
						} catch (Exception e) {
							log.error("Message processing failed", e);
						}
						offset = update.getUpdateId() + 1;
					}
				}

				Thread.sleep(1000);
			}
		});
	}

}
