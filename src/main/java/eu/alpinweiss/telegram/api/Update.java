package eu.alpinweiss.telegram.api;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Update {
	@SerializedName("update_id")
	private Long updateId;
	private Message message;
}
