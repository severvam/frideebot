package eu.alpinweiss.telegram.api;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.Date;

@Data
public class Message {
	@SerializedName("message_id")
	private Long messageId;
	private User from;
	private Chat chat;
	private Date date;
	private String text;
}
