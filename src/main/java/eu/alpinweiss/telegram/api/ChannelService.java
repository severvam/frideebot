package eu.alpinweiss.telegram.api;

import eu.alpinweiss.telegram.persistence.fridee.*;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class ChannelService {

	private final FrideeBoobieDao frideeBoobieDao;
	private final FrideeVideoDao frideeVideoDao;
	private final TelegramBotApi telegramBotApi;
	private final FrideeDao frideeDao;

	@Inject
	public ChannelService(FrideeBoobieDao frideeBoobieDao, FrideeVideoDao frideeVideoDao,
	                      TelegramBotApi telegramBotApi, FrideeDao frideeDao) {
		this.frideeBoobieDao = frideeBoobieDao;
		this.frideeVideoDao = frideeVideoDao;
		this.telegramBotApi = telegramBotApi;
		this.frideeDao = frideeDao;
	}

	public void postPhotos(int limit, long chat) {
		final var boobies = frideeBoobieDao.readLatest(limit);
		final var frideeId = boobies.stream().findFirst().map(FrideeBoobie::getFrideeId).orElseThrow();

		final var videos = frideeVideoDao.readLatest(frideeId);
		final var fridee = frideeDao.find(frideeId).orElseThrow();

		videos.forEach(video -> {
			final var message = String.format("[Видео с %s](%s)", fridee.getTitle(), video.getUrl());
			telegramBotApi.sendVideo(chat, message);
			frideeVideoDao.markPosted(video.getId());
		});

		boobies.forEach(boobie -> {
			final var message = String.format("Фотография с %s", fridee.getTitle());
			telegramBotApi.uploadImage(chat, message, boobie.getUrl());
			frideeBoobieDao.markPosted(boobie.getId());
		});
	}

}
