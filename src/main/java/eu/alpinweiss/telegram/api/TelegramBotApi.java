package eu.alpinweiss.telegram.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.alpinweiss.telegram.api.response.MessageResponse;
import eu.alpinweiss.telegram.api.response.MyInfoResponse;
import eu.alpinweiss.telegram.api.response.PhotoUploadResponse;
import eu.alpinweiss.telegram.api.response.UpdateResponse;
import eu.alpinweiss.telegram.config.TelegramBotGateway;
import eu.alpinweiss.telegram.config.UnixDateTimeTypeAdapter;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

@Slf4j
@Singleton
public class TelegramBotApi {

	private final TelegramBotGateway telegramBotGateway;
	private final Gson gson;

	@Inject
	public TelegramBotApi(TelegramBotGateway telegramBotGateway) {
		this.telegramBotGateway = telegramBotGateway;
		this.gson = new GsonBuilder().registerTypeAdapter(Date.class, new UnixDateTimeTypeAdapter()).create();
	}

	public Optional<UpdateResponse> getUpdates(long offset) {
		try {
			final var botInfo = telegramBotGateway.callGet("getUpdates?offset=" + offset);
			return of(gson.fromJson(botInfo, UpdateResponse.class));
		} catch (RuntimeException e) {
			log.error("Error getting updates", e);
		}
		return empty();
	}

	public Optional<MyInfoResponse> getMyInfo() {
		try {
			return of(gson.fromJson(telegramBotGateway.callGet("getMe"), MyInfoResponse.class));
		} catch (RuntimeException e) {
			log.error("Error getting bot information", e);
		}
		return empty();
	}

	public Optional<MessageResponse> sendMessage(long chatId, String message) {
		return sendMessageWithErrorParameter(chatId, message, "Error sending message");
	}

	public Optional<MessageResponse> sendVideo(long chatId, String message) {
		return sendMessageWithErrorParameter(chatId, message, "Video send failed");
	}

	public Optional<PhotoUploadResponse> uploadImage(long chatId, String message, String image) {
		try {
			final var answer = telegramBotGateway.callGet("sendPhoto?chat_id=" + chatId + "&parse_mode=Markdown&caption=" + encode(message) + "&photo=" + encode(image));
			return of(gson.fromJson(answer, PhotoUploadResponse.class));
		} catch (RuntimeException e) {
			log.error("Error uploading image", e);
		}
		return empty();
	}


	private Optional<MessageResponse> sendMessageWithErrorParameter(long chatId, String message, String errorMessage) {
		try {
			final var answer = telegramBotGateway.callGet("sendMessage?chat_id=" + chatId + "&parse_mode=Markdown&text=" + encode(message));
			return of(gson.fromJson(answer, MessageResponse.class));
		} catch (RuntimeException e) {
			log.error(errorMessage, e);
		}
		return empty();
	}

	private String encode(String message) {
		return URLEncoder.encode(message, StandardCharsets.UTF_8);
	}

}
