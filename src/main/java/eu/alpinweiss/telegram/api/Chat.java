package eu.alpinweiss.telegram.api;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Chat {
	private Long id;
	@SerializedName("first_name")
	private String firstName;
	private String username;
	private String type;
}
