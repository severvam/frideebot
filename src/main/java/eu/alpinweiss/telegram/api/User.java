package eu.alpinweiss.telegram.api;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class User {
	private Long id;
	@SerializedName("is_bot")
	private Boolean bot;
	@SerializedName("first_name")
	private String firstName;
	private String username;
	@SerializedName("language_code")
	private String languageCode;
}
