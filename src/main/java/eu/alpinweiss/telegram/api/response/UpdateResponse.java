package eu.alpinweiss.telegram.api.response;

import eu.alpinweiss.telegram.api.Update;
import lombok.Data;

import java.util.List;

@Data
public class UpdateResponse {
	private String ok;
	private List<Update> result;
}
