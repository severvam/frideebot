package eu.alpinweiss.telegram.api.response;

import eu.alpinweiss.telegram.api.User;
import lombok.Data;

@Data
public class MyInfoResponse {
	private String ok;
	private User result;
}
