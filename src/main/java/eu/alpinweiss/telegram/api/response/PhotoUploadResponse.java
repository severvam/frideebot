package eu.alpinweiss.telegram.api.response;

import eu.alpinweiss.telegram.api.PhotoUpload;
import lombok.Data;

@Data
public class PhotoUploadResponse {
	private String ok;
	private PhotoUpload result;
}
