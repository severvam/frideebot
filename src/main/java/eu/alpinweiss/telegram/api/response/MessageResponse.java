package eu.alpinweiss.telegram.api.response;

import eu.alpinweiss.telegram.api.Message;
import lombok.Data;

@Data
public class MessageResponse {
	private String ok;
	private Message result;
}
