package eu.alpinweiss.telegram.api;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Photo {
	@SerializedName("file_id")
	private String id;
	@SerializedName("file_size")
	private Long size;
	private Integer width;
	private Integer height;
}
