package eu.alpinweiss.telegram.api;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class PhotoUpload {
	@SerializedName("message_id")
	private Long id;
	private User from;
	private Chat chat;
	private Date date;
	private String caption;
	@SerializedName("photo")
	private List<Photo> photos;
}
