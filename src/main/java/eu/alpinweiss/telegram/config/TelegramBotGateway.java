package eu.alpinweiss.telegram.config;

import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Singleton
@Slf4j
public class TelegramBotGateway {

	private final String botUrl = "https://api.telegram.org/";
	private String token;

	public TelegramBotGateway() {
		this.token = System.getenv("TELEGRAM_TOKEN");
	}

	public String callGet(String method) {
		return get(botUrl + "bot" + token + "/" + method);
	}

	public String get(String botMethod) {
		String answer = null;
		try {
			final var url = new URL(botMethod);

			final var conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				log.error("Failed: HTTP error code: " + conn.getResponseCode() + " Message: " + conn.getResponseMessage());
				log.error("Request: " + botMethod);
				throw new RestApiException("Failed: HTTP error code: " + conn.getResponseCode());
			}

			answer = getResponse(conn);
			conn.disconnect();

		} catch (IOException e) {
			log.error("Failed GET call", e);
		}

		return answer;
	}

	public String post(String botMethod, String payload) {
		String answer = null;
		try {
			final var url = new URL(botMethod);
			final var conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");

			final var os = conn.getOutputStream();
			os.write(payload.getBytes());
			os.flush();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				log.error("Failed: HTTP error code: " + conn.getResponseCode() + " Message: " + conn.getResponseMessage());
				log.error("Request: " + botMethod);
				throw new RestApiException("Failed: HTTP error code: " + conn.getResponseCode());
			}

			answer = getResponse(conn);
			conn.disconnect();
		} catch (IOException e) {
			log.error("Failed POST call", e);
		}
		return answer;
	}


	private String getResponse(HttpURLConnection conn) throws IOException {
		final var br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		final var stringBuilder = new StringBuilder();
		String output;
		while ((output = br.readLine()) != null) {
			stringBuilder.append(output);
		}
		return stringBuilder.toString();
	}

}
