package eu.alpinweiss.telegram.config;

import javax.inject.Singleton;

import static java.util.Optional.ofNullable;

@Singleton
public class ChatConfigService {

	private long definedChat = 0;

	public long chatId() {
		if (definedChat == 0) {
			final var testTelegramChat = ofNullable(System.getenv("TEST_TELEGRAM_CHAT")).orElse("");
			if (testTelegramChat.isBlank()) {
				throw new ChatNotConfiguredException("Please configure TEST_TELEGRAM_CHAT environment variable");
			} else {
				definedChat = Long.parseLong(testTelegramChat);
			}
		}
		return definedChat;
	}

}
