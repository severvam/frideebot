package eu.alpinweiss.telegram.config;

public class RestApiException extends RuntimeException {

	public RestApiException(String message) {
		super(message);
	}
}
