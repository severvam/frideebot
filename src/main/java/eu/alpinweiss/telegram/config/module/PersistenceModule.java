package eu.alpinweiss.telegram.config.module;

import dagger.Module;
import dagger.Provides;
import eu.alpinweiss.telegram.persistence.DatabaseConfigInitializer;
import eu.alpinweiss.telegram.persistence.fridee.FrideeBoobieDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeVideoDao;

@Module
public class PersistenceModule {

	@Provides
	static FrideeBoobieDao frideeBoobieDao(DatabaseConfigInitializer databaseConfigInitializer) {
		return new FrideeBoobieDao(databaseConfigInitializer);
	}

	@Provides
	static FrideeVideoDao frideeVideoDao(DatabaseConfigInitializer databaseConfigInitializer) {
		return new FrideeVideoDao(databaseConfigInitializer);
	}

	@Provides
	static FrideeDao frideeDao(DatabaseConfigInitializer databaseConfigInitializer) {
		return new FrideeDao(databaseConfigInitializer);
	}

	@Provides
	static DatabaseConfigInitializer databaseConfigInitializer() {
		return new DatabaseConfigInitializer();
	}

}
