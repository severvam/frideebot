package eu.alpinweiss.telegram.config.module;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;
import eu.alpinweiss.telegram.chat.*;
import eu.alpinweiss.telegram.persistence.fridee.FrideeBoobieDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeVideoDao;

@Module
public class CommandModule {

	@Provides
	@IntoSet
	static Command frideeBoobiesCommand(FrideeDao frideeDao, FrideeBoobieDao frideeBoobieDao) {
		return new FrideeBoobiesCommand(frideeDao, frideeBoobieDao);
	}

	@Provides
	@IntoSet
	static Command fullFrideeListCommand(FrideeDao frideeDao) {
		return new FullFrideeListCommand(frideeDao);
	}

	@Provides
	@IntoSet
	static Command helpCommand() {
		return new HelpCommand();
	}

	@Provides
	@IntoSet
	static Command lastFrideeCommand(FrideeDao frideeDao) {
		return new LastFrideeCommand(frideeDao);
	}

	@Provides
	@IntoSet
	static Command lastNFrideeListCommand(FrideeDao frideeDao) {
		return new LastNFrideeListCommand(frideeDao);
	}

	@Provides
	@IntoSet
	static Command randomPhotoCommand(FrideeDao frideeDao, FrideeBoobieDao frideeBoobieDao) {
		return new RandomPhotoCommand(frideeDao, frideeBoobieDao);
	}

	@Provides
	@IntoSet
	static Command randomVideoCommand(FrideeDao frideeDao, FrideeVideoDao frideeVideoDao) {
		return new RandomVideoCommand(frideeDao, frideeVideoDao);
	}

}
