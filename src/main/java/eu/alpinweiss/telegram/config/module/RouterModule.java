package eu.alpinweiss.telegram.config.module;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;
import eu.alpinweiss.telegram.RouteHandler;
import eu.alpinweiss.telegram.api.ChannelService;
import eu.alpinweiss.telegram.api.TelegramBotApi;
import eu.alpinweiss.telegram.config.ChatConfigService;
import eu.alpinweiss.telegram.scraper.FrideeScraperService;
import eu.alpinweiss.telegram.web.resources.*;

@Module
public class RouterModule {

	@Provides
	@IntoSet
	static RouteHandler callGetUpdatesResourceHandler(TelegramBotApi telegramBotApi) {
		return new CallGetUpdatesResource(telegramBotApi);
	}

	@Provides
	@IntoSet
	static RouteHandler callMyInfoResourceHandler(TelegramBotApi telegramBotApi) {
		return new CallMyInfoResource(telegramBotApi);
	}

	@Provides
	@IntoSet
	static RouteHandler callSendMessageResourceHandler(TelegramBotApi telegramBotApi) {
		return new CallSendMessageResource(telegramBotApi);
	}

	@Provides
	@IntoSet
	static RouteHandler postDailyFrideeResourceHandler(ChannelService channelService, ChatConfigService chatConfigService) {
		return new PostDailyFrideeResource(channelService, chatConfigService);
	}

	@Provides
	@IntoSet
	static RouteHandler postFrideeResourceHandler(ChannelService channelService, ChatConfigService chatConfigService) {
		return new PostFrideeResource(channelService, chatConfigService);
	}

	@Provides
	@IntoSet
	static RouteHandler scrapeFrideeHandler(FrideeScraperService frideeScraperService) {
		return new ScrapeFrideeResource(frideeScraperService);
	}

	@Provides
	@IntoSet
	static RouteHandler scrapeNewFrideesHandler(FrideeScraperService frideeScraperService) {
		return new ScrapeNewFrideesResource(frideeScraperService);
	}

	@Provides
	@IntoSet
	static RouteHandler webHookResourceHandler() {
		return new WebHookResource();
	}

}
