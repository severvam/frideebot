package eu.alpinweiss.telegram.config.module;

import dagger.Module;
import dagger.Provides;
import eu.alpinweiss.telegram.api.ChannelService;
import eu.alpinweiss.telegram.api.TelegramBotApi;
import eu.alpinweiss.telegram.chat.Command;
import eu.alpinweiss.telegram.chat.CommandExecutor;
import eu.alpinweiss.telegram.config.ChatConfigService;
import eu.alpinweiss.telegram.config.TelegramBotGateway;
import eu.alpinweiss.telegram.persistence.fridee.FrideeBoobieDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeVideoDao;
import eu.alpinweiss.telegram.scraper.FrideeScraperService;

import java.util.Set;

@Module
public class ServiceModule {

	@Provides
	static ChannelService channelService(FrideeBoobieDao frideeBoobieDao, FrideeVideoDao frideeVideoDao,
	                                     TelegramBotApi telegramBotApi, FrideeDao frideeDao) {
		return new ChannelService(frideeBoobieDao, frideeVideoDao, telegramBotApi, frideeDao);
	}

	@Provides
	static ChatConfigService chatConfigService() {
		return new ChatConfigService();
	}

	@Provides
	static TelegramBotApi telegramBotApi(TelegramBotGateway telegramBotGateway) {
		return new TelegramBotApi(telegramBotGateway);
	}

	@Provides
	static TelegramBotGateway telegramBotGateway() {
		return new TelegramBotGateway();
	}

	@Provides
	static CommandExecutor commandExecutor(Set<Command> commands, TelegramBotApi telegramBotApi) {
		return new CommandExecutor(commands, telegramBotApi);
	}

	@Provides
	static FrideeScraperService frideeScraperService(FrideeDao frideeDao, FrideeBoobieDao frideeBoobieDao, FrideeVideoDao frideeVideoDao) {
		return new FrideeScraperService(frideeDao, frideeBoobieDao, frideeVideoDao);
	}

}
