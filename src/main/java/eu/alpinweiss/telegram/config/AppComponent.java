package eu.alpinweiss.telegram.config;

import dagger.Component;
import eu.alpinweiss.telegram.RouteHandler;
import eu.alpinweiss.telegram.api.ChannelService;
import eu.alpinweiss.telegram.api.TelegramBotApi;
import eu.alpinweiss.telegram.chat.Command;
import eu.alpinweiss.telegram.chat.CommandExecutor;
import eu.alpinweiss.telegram.config.module.CommandModule;
import eu.alpinweiss.telegram.config.module.PersistenceModule;
import eu.alpinweiss.telegram.config.module.RouterModule;
import eu.alpinweiss.telegram.config.module.ServiceModule;
import eu.alpinweiss.telegram.persistence.DatabaseConfigInitializer;
import eu.alpinweiss.telegram.persistence.fridee.FrideeBoobieDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeVideoDao;
import eu.alpinweiss.telegram.scraper.FrideeScraperService;

import java.util.Set;

@Component(modules = {
		RouterModule.class,
		CommandModule.class,
		ServiceModule.class,
		PersistenceModule.class
})
public interface AppComponent {

	ChannelService getChannelService();

	TelegramBotApi getTelegramBotApi();

	CommandExecutor getCommandExecutor();

	ChatConfigService getChatConfigService();

	TelegramBotGateway getTelegramBotGateway();

	FrideeDao getFrideeDao();

	FrideeBoobieDao getFrideeBoobieDao();

	FrideeVideoDao getFrideeVideoDao();

	DatabaseConfigInitializer getDatabaseConfigInitializer();

	FrideeScraperService getFrideeScraperService();

	Set<RouteHandler> getRouteHandlers();

	Set<Command> getBotCommands();

}
