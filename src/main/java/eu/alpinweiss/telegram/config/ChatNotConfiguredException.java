package eu.alpinweiss.telegram.config;

public class ChatNotConfiguredException extends RuntimeException {

	public ChatNotConfiguredException(String message) {
		super(message);
	}

}
