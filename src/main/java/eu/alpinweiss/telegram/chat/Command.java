package eu.alpinweiss.telegram.chat;

public interface Command {

	default boolean isCommandNullOrEmpty(String command) {
		return (command == null || "".equals(command.trim()));
	}

	boolean applicable(String command);

	CommandResult execute(String command);

}
