package eu.alpinweiss.telegram.chat;

import eu.alpinweiss.telegram.persistence.fridee.FrideeDao;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.regex.Pattern;

@Slf4j
@Singleton
public class LastNFrideeListCommand implements Command {

	private final FrideeDao frideeDao;
	private final Pattern regex;

	@Inject
	public LastNFrideeListCommand(FrideeDao frideeDao) {
		this.frideeDao = frideeDao;
		this.regex = Pattern.compile("\\d+");
	}

	@Override
	public boolean applicable(String command) {
		return !isCommandNullOrEmpty(command) && command.trim().toLowerCase().matches("/последние \\d+");
	}

	@Override
	public CommandResult execute(String command) {
		log.info("Requested '" + command + "'");
		final var matcher = regex.matcher(command);
		if (matcher.find()) {
			final var fridees = frideeDao.frideeTitleList(Integer.parseInt(matcher.group()));
			return CommandResult.message(String.join("\n", fridees));
		}
		return CommandResult.message("Упс, не понял.");
	}

}
