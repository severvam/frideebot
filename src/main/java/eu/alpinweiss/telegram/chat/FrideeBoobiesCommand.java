package eu.alpinweiss.telegram.chat;

import eu.alpinweiss.telegram.persistence.fridee.FrideeBoobie;
import eu.alpinweiss.telegram.persistence.fridee.FrideeBoobieDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeDao;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.regex.Pattern;

import static eu.alpinweiss.telegram.chat.CommandType.IMAGE;
import static java.util.stream.Collectors.toList;

@Slf4j
@Singleton
public class FrideeBoobiesCommand implements Command {

	private final FrideeDao frideeDao;
	private final FrideeBoobieDao frideeBoobieDao;
	private final Pattern regex;

	@Inject
	public FrideeBoobiesCommand(FrideeDao frideeDao, FrideeBoobieDao frideeBoobieDao) {
		this.frideeDao = frideeDao;
		this.frideeBoobieDao = frideeBoobieDao;
		this.regex = Pattern.compile("\\d+");
	}

	@Override
	public boolean applicable(String command) {
		return !isCommandNullOrEmpty(command) && command.trim().toLowerCase().matches("/пятниццо #\\d+");
	}

	@Override
	public CommandResult execute(String command) {
		log.info("Requested '" + command + "'");
		final var matcher = regex.matcher(command);
		if (matcher.find()) {
			final var fridee = frideeDao.findByFrideeTitleNumber(matcher.group());
			final var boobies = frideeBoobieDao.findByFridee(fridee);
			return new CommandResult(IMAGE, String.format("Фотография с %s", fridee.getTitle()), boobies.stream().map(FrideeBoobie::getUrl).collect(toList()));
		}
		return CommandResult.message("Упс, не понял.");
	}

}