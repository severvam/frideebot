package eu.alpinweiss.telegram.chat;

import eu.alpinweiss.telegram.persistence.fridee.FrideeDao;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;

@Slf4j
@Singleton
public class FullFrideeListCommand implements Command {

	private final FrideeDao frideeDao;

	@Inject
	public FullFrideeListCommand(FrideeDao frideeDao) {
		this.frideeDao = frideeDao;
	}

	@Override
	public boolean applicable(String command) {
		return !isCommandNullOrEmpty(command) && "/список".equals(command.trim().toLowerCase());
	}

	@Override
	public CommandResult execute(String command) {
		log.info("Requested '" + command + "'");
		final var fridees = frideeDao.frideeTitleList();
		return CommandResult.message(String.join("\n ", fridees));
	}

}
