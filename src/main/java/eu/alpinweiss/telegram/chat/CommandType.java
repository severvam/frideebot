package eu.alpinweiss.telegram.chat;

public enum CommandType {
	MESSAGE, IMAGE, VIDEO
}
