package eu.alpinweiss.telegram.chat;

import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@Slf4j
@Singleton
public class HelpCommand implements Command {

	@Override
	public boolean applicable(String command) {
		return !isCommandNullOrEmpty(command) &&
				("/help".equals(command.trim().toLowerCase()) || "/start".equals(command.trim().toLowerCase()));
	}

	@Override
	public CommandResult execute(String command) {
		log.info("Requested '" + command + "'");
		return CommandResult.message("Доступные команды:\n\n\n*/help* - справка\n*/start* - справка" +
				"\n\n*/пятниццо* - вернет случайную фотографию из всех доступных Пятницц" +
				"\n\n*/пятниццо #<номер пятниццы>* - вернет весь сет фотографий запрошенной пятниццы.\nПример команды: `/пятниццо #362`" +
				"\n\n*/список* - вернет все доступные пятниццы" +
				"\n\n*/последняя* - вернет последнюю пятниццу" +
				"\n\n*/последние <количество пятницц>* - вернет последние N пятницц\nПример команды: `/последние 5`" +
				"\n\n*/видео* - вернет случайное видео");
	}

}
