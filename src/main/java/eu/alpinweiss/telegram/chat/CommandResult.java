package eu.alpinweiss.telegram.chat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.List.of;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommandResult {

	private CommandType commandType;
	private String message;
	private List<String> boobsImages;

	public static CommandResult message(String message) {
		return new CommandResult(CommandType.MESSAGE, message, null);
	}

	public static CommandResult image(String message, String url) {
		return new CommandResult(CommandType.IMAGE, message, of(url));
	}

	public static CommandResult video(String message) {
		return new CommandResult(CommandType.VIDEO, message, null);
	}

}
