package eu.alpinweiss.telegram.chat;

import eu.alpinweiss.telegram.api.TelegramBotApi;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Set;

@Slf4j
@Singleton
public class CommandExecutor {

	private final Set<Command> commands;
	private final TelegramBotApi telegramBotApi;

	@Inject
	public CommandExecutor(Set<Command> commands, TelegramBotApi telegramBotApi) {
		this.commands = commands;
		this.telegramBotApi = telegramBotApi;
	}

	public void executeCommand(String message, long chatId) {
		var commandExecuted = false;
		for (Command command : commands) {
			if (command.applicable(message)) {
				final var answer = command.execute(message);
				switch (answer.getCommandType()) {
					case IMAGE:
						uploadImage(chatId, answer);
						break;
					case VIDEO:
						telegramBotApi.sendVideo(chatId, answer.getMessage());
						break;
					case MESSAGE:
					default:
						telegramBotApi.sendMessage(chatId, answer.getMessage());
						break;
				}
				commandExecuted = true;
				break;
			}
		}
		if (!commandExecuted) {
			log.info("Unknown command requested: '" + message + "'");
			telegramBotApi.sendMessage(chatId, "Я не понял команду, набери /help для помощи.");
		}
	}

	private void uploadImage(long chatId, CommandResult answer) {
		answer.getBoobsImages().forEach(image -> telegramBotApi.uploadImage(chatId, answer.getMessage(), image));
	}

}
