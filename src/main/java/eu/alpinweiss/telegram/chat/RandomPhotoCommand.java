package eu.alpinweiss.telegram.chat;

import eu.alpinweiss.telegram.persistence.fridee.FrideeBoobieDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeDao;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;

@Slf4j
@Singleton
public class RandomPhotoCommand implements Command {

	private final FrideeDao frideeDao;
	private final FrideeBoobieDao frideeBoobieDao;

	@Inject
	public RandomPhotoCommand(FrideeDao frideeDao, FrideeBoobieDao frideeBoobieDao) {
		this.frideeDao = frideeDao;
		this.frideeBoobieDao = frideeBoobieDao;
	}

	@Override
	public boolean applicable(String command) {
		return !isCommandNullOrEmpty(command) && "/пятниццо".equals(command.trim().toLowerCase());
	}

	@Override
	public CommandResult execute(String command) {
		log.info("Requested '" + command + "'");
		final var frideeBoobie = frideeBoobieDao.randomPhoto();
		final var fridee = frideeDao.find(frideeBoobie.getFrideeId()).orElseThrow();
		return CommandResult.image(String.format("Фотография с %s", fridee.getTitle()), frideeBoobie.getUrl());
	}

}
