package eu.alpinweiss.telegram.chat;

import eu.alpinweiss.telegram.persistence.fridee.FrideeDao;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;

@Slf4j
@Singleton
public class LastFrideeCommand implements Command {

	private final FrideeDao frideeDao;

	@Inject
	public LastFrideeCommand(FrideeDao frideeDao) {
		this.frideeDao = frideeDao;
	}

	@Override
	public boolean applicable(String command) {
		return !isCommandNullOrEmpty(command) && "/последняя".equals(command.trim().toLowerCase());
	}

	@Override
	public CommandResult execute(String command) {
		log.info("Requested '" + command + "'");
		final var fridees = frideeDao.frideeTitleList(1);
		return CommandResult.message(String.join("\n ", fridees));
	}

}
