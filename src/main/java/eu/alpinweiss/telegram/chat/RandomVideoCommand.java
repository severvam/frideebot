package eu.alpinweiss.telegram.chat;

import eu.alpinweiss.telegram.persistence.fridee.FrideeDao;
import eu.alpinweiss.telegram.persistence.fridee.FrideeVideoDao;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;

@Slf4j
@Singleton
public class RandomVideoCommand implements Command {

	private final FrideeDao frideeDao;
	private final FrideeVideoDao frideeVideoDao;

	@Inject
	public RandomVideoCommand(FrideeDao frideeDao, FrideeVideoDao frideeVideoDao) {
		this.frideeDao = frideeDao;
		this.frideeVideoDao = frideeVideoDao;
	}

	@Override
	public boolean applicable(String command) {
		return !isCommandNullOrEmpty(command) && "/видео".equals(command.trim().toLowerCase());
	}

	@Override
	public CommandResult execute(String command) {
		log.info("Requested '" + command + "'");
		final var frideeVideo = frideeVideoDao.randomVideo();
		final var fridee = frideeDao.find(frideeVideo.getFrideeId()).orElseThrow();
		return CommandResult.video(String.format("[Видео с %s](%s)", fridee.getTitle(), frideeVideo.getUrl()));
	}

}
