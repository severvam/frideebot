package eu.alpinweiss.telegram;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;

public interface RouteHandler {

	String CONTENT_TYPE = "content-type";
	String APPLICATION_JSON_TYPE = "application/json";
	String TEXT_PLAIN_TYPE = "text/plain";

	String route();

	Handler<RoutingContext> handler();

	default HttpMethod method() {
		return HttpMethod.GET;
	}

}
