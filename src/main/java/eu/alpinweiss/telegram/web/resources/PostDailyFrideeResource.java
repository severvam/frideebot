package eu.alpinweiss.telegram.web.resources;

import eu.alpinweiss.telegram.api.ChannelService;
import eu.alpinweiss.telegram.config.ChatConfigService;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PostDailyFrideeResource extends AbstractPostFrideeResource {

	@Inject
	public PostDailyFrideeResource(ChannelService channelService, ChatConfigService chatConfigService) {
		super(channelService, chatConfigService);
	}

	@Override
	public String route() {
		return "/postDaily";
	}

	@Override
	int getLimit() {
		return 3;
	}

}
