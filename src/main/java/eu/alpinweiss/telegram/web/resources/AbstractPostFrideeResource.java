package eu.alpinweiss.telegram.web.resources;

import eu.alpinweiss.telegram.RouteHandler;
import eu.alpinweiss.telegram.api.ChannelService;
import eu.alpinweiss.telegram.config.ChatConfigService;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

public abstract class AbstractPostFrideeResource implements RouteHandler {

	private final ChannelService channelService;
	private final ChatConfigService chatConfigService;

	AbstractPostFrideeResource(ChannelService channelService, ChatConfigService chatConfigService) {
		this.channelService = channelService;
		this.chatConfigService = chatConfigService;
	}

	@Override
	public Handler<RoutingContext> handler() {
		return (routingContext) ->
				routingContext.vertx().executeBlocking(future -> {
					channelService.postPhotos(getLimit(), chatConfigService.chatId());
					future.complete();
				}, res -> {
					final var response = routingContext.response();
					response.putHeader(CONTENT_TYPE, TEXT_PLAIN_TYPE);
					response.end("Ok");
				});
	}

	abstract int getLimit();

}
