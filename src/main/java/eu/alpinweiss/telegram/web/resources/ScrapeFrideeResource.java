package eu.alpinweiss.telegram.web.resources;

import eu.alpinweiss.telegram.RouteHandler;
import eu.alpinweiss.telegram.scraper.FrideeScraperService;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ScrapeFrideeResource implements RouteHandler {

	private final FrideeScraperService frideeScraperService;

	@Inject
	public ScrapeFrideeResource(FrideeScraperService frideeScraperService) {
		this.frideeScraperService = frideeScraperService;
	}

	@Override
	public String route() {
		return "/scrape/:id";
	}

	@Override
	public Handler<RoutingContext> handler() {
		return (routingContext) ->
				routingContext.vertx().executeBlocking(future -> {
					final var id = routingContext.request().getParam("id");
					frideeScraperService.scrapeAndStore(id);
					future.complete();
				}, res -> {
					final var response = routingContext.response();
					response.putHeader(CONTENT_TYPE, TEXT_PLAIN_TYPE);
					response.end("Done!");
				});
	}

}
