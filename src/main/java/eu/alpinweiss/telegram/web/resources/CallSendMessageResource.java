package eu.alpinweiss.telegram.web.resources;

import eu.alpinweiss.telegram.RouteHandler;
import eu.alpinweiss.telegram.api.TelegramBotApi;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class CallSendMessageResource implements RouteHandler {

	private final TelegramBotApi telegramBotApi;

	@Inject
	public CallSendMessageResource(TelegramBotApi telegramBotApi) {
		this.telegramBotApi = telegramBotApi;
	}

	@Override
	public String route() {
		return "/send-message";
	}

	@Override
	public Handler<RoutingContext> handler() {
		return (routingContext) -> {
			final var response = routingContext.response();
			final var botInfo = telegramBotApi.sendMessage(90189182L, "Йоба, как тебе?");

			response.putHeader(CONTENT_TYPE, APPLICATION_JSON_TYPE);
			botInfo.ifPresentOrElse(info -> response.end(info.toString()), () -> response.end("Message send failed"));
		};
	}

}
