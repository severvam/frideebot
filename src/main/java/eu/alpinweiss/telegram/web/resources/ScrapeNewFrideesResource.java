package eu.alpinweiss.telegram.web.resources;

import eu.alpinweiss.telegram.RouteHandler;
import eu.alpinweiss.telegram.scraper.FrideeScraperService;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ScrapeNewFrideesResource implements RouteHandler {

	private final FrideeScraperService frideeScraperService;

	@Inject
	public ScrapeNewFrideesResource(FrideeScraperService frideeScraperService) {
		this.frideeScraperService = frideeScraperService;
	}

	@Override
	public String route() {
		return "/new";
	}

	@Override
	public Handler<RoutingContext> handler() {
		return (routingContext) ->
				routingContext.vertx().executeBlocking(future -> {
					frideeScraperService.scrapeAndStoreNewFridees();
					future.complete();
				}, res -> {
					final var response = routingContext.response();
					response.putHeader(CONTENT_TYPE, TEXT_PLAIN_TYPE);
					response.end("Finished!");
				});
	}

}
