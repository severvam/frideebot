package eu.alpinweiss.telegram.web.resources;

import eu.alpinweiss.telegram.RouteHandler;
import eu.alpinweiss.telegram.api.TelegramBotApi;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class CallGetUpdatesResource implements RouteHandler {

	private final TelegramBotApi telegramBotApi;

	@Inject
	public CallGetUpdatesResource(TelegramBotApi telegramBotApi) {
		this.telegramBotApi = telegramBotApi;
	}

	@Override
	public String route() {
		return "/get-updates";
	}

	@Override
	public Handler<RoutingContext> handler() {
		return (routingContext) -> {
			final var response = routingContext.response();
			final var updateResponse = telegramBotApi.getUpdates(1L);

			response.putHeader(CONTENT_TYPE, APPLICATION_JSON_TYPE);
			updateResponse.ifPresentOrElse(update -> response.end(update.toString()), () -> response.end("Update receive failed"));
		};
	}

}
