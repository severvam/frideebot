package eu.alpinweiss.telegram.web.resources;

import eu.alpinweiss.telegram.api.ChannelService;
import eu.alpinweiss.telegram.config.ChatConfigService;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PostFrideeResource extends AbstractPostFrideeResource {

	@Inject
	public PostFrideeResource(ChannelService channelService, ChatConfigService chatConfigService) {
		super(channelService, chatConfigService);
	}

	@Override
	int getLimit() {
		return 6;
	}

	@Override
	public String route() {
		return "/postFridee";
	}

}
