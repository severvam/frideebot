package eu.alpinweiss.telegram.web.resources;

import eu.alpinweiss.telegram.RouteHandler;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;

public class WebHookResource implements RouteHandler {

	@Override
	public String route() {
		return "/bot/web-hook";
	}

	@Override
	public HttpMethod method() {
		return HttpMethod.POST;
	}

	@Override
	public Handler<RoutingContext> handler() {
		return (routingContext) -> {
			final var body = routingContext.getBodyAsString();
			final var response = routingContext.response();
			response.putHeader(CONTENT_TYPE, APPLICATION_JSON_TYPE);
			response.end(body);
		};
	}

}
