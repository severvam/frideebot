package eu.alpinweiss.telegram;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

import static java.lang.String.format;

@Slf4j
class RouteInitializer {

	void initRoutes(Router router, Set<RouteHandler> routeHandlers) {
		final StringBuilder routes = new StringBuilder("Routing configuration:\n");
		routeHandlers.forEach(route -> {
			router.route(route.method(), route.route()).handler(route.handler());
			routes.append(format("Added route %s\n", route.route()));
		});
		log.info(routes.toString());
	}

	Handler<AsyncResult<HttpServer>> listenHandler(int port) {
		return res -> {
			if (res.succeeded()) {
				log.info(format("http://localhost:%d/", port));
			} else {
				log.error(format("Failed to listen on port %d", port));
			}
		};
	}
}
