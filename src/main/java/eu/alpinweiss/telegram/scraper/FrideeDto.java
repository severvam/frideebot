package eu.alpinweiss.telegram.scraper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
class FrideeDto {
	private Integer id;
	private String title;
	private Date date;
	private List<VideoDto> videos;
	private List<String> photos;
}
