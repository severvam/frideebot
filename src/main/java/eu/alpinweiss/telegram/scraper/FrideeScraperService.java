package eu.alpinweiss.telegram.scraper;

import eu.alpinweiss.telegram.persistence.fridee.*;

import javax.inject.Inject;
import javax.inject.Singleton;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Singleton
public class FrideeScraperService {

	private final FrideeScraper frideeScraper = new FrideeScraper();

	private final FrideeDao frideeDao;
	private final FrideeBoobieDao frideeBoobieDao;
	private final FrideeVideoDao frideeVideoDao;

	@Inject
	public FrideeScraperService(FrideeDao frideeDao, FrideeBoobieDao frideeBoobieDao, FrideeVideoDao frideeVideoDao) {
		this.frideeDao = frideeDao;
		this.frideeBoobieDao = frideeBoobieDao;
		this.frideeVideoDao = frideeVideoDao;
	}


	public void scrapeAndStoreNewFridees() {
		final var frideeDtos = frideeScraper.scrapePosts(FrideeScraper.BASE_URL + "/users/J1mm/shouts");
		final var filteredFridees = frideeDtos.stream()
				.filter(frideeDto -> isFrideePost(frideeDto.getTitle()))
				.collect(toList());

		final var frideeList = frideeDao.findStoredPosts(filteredFridees.stream().map(FrideeDto::getTitle).collect(toList()));
		final var storedFrideeTitles = frideeList.stream().map(Fridee::getTitle).collect(toSet());
		for (FrideeDto filteredFridee : filteredFridees) {
			if (!storedFrideeTitles.contains(filteredFridee.getTitle())) {
				scrapeAndStore(filteredFridee.getId().toString());
			}
		}
	}

	public void scrapeAndStore(String id) {
		final var url = FrideeScraper.BASE_URL + "/shouts/" + id;
		final var frideeDto = frideeScraper.scrape(url);
		final var fridee = Fridee.init(frideeDto.getTitle(), url, frideeDto.getDate());

		final var transaction = frideeDao.startTransaction();
		frideeDao.saveInTransaction(fridee, transaction);

		final var frideeBoobies = frideeDto.getPhotos().stream()
				.map(photoUrl -> FrideeBoobie.init(fridee.getId(), photoUrl))
				.collect(toList());
		frideeBoobieDao.saveAllInTransaction(frideeBoobies, transaction);

		final var frideeVideos = frideeDto.getVideos().stream()
				.map(videoDto -> FrideeVideo.init(fridee.getId(), videoDto.fullUrl()))
				.collect(toList());
		frideeVideoDao.saveAllInTransaction(frideeVideos, transaction);

		transaction.commit();
	}

	private boolean isFrideePost(String title) {
		return title != null && (title.toLowerCase().startsWith("пятниццо"));
	}

}
