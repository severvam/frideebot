package eu.alpinweiss.telegram.scraper;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Slf4j
class FrideeScraper {

	final static String BASE_URL = "http://autokadabra.ru";
	private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

	public FrideeDto scrape(String url) {
		try {
			final var document = Jsoup.connect(url).get();
			final var boobsList = new ArrayList<String>();
			final var videoList = new ArrayList<VideoDto>();

			final var title = document.select("h1.shout_title").text();
			final var date = dateFormat.parse(document.select("time").attr("datetime"));

			document.select("div.preview_big > img").forEach(element -> addBaseUrlAsPrefix(boobsList, element.attr("src")));
			document.select("div.shout")
					.forEach(element -> element.select("div.photo_gallery img")
							.forEach(img -> addBaseUrlAsPrefix(boobsList, BASE_URL + img.attr("data-big"))));

			document.select("div.embed_video").forEach(element -> videoList.add(new VideoDto(element.attr("data-provider"), element.attr("data-id"))));

			if (boobsList.isEmpty()) {
				document.select("div.shout > div.post_body > div.article > div.text > div.text img").forEach(element -> {
					if (!"thumb".equals(element.className())) {
						addBaseUrlAsPrefix(boobsList, element.attr("src"));
					}
				});
			}

			return new FrideeDto(null, title, date, videoList, boobsList);
		} catch (IOException | ParseException e) {
			log.error("Error scrape", e);
			throw new ScrapeException(e);
		}
	}

	private void addBaseUrlAsPrefix(ArrayList<String> boobsList, String src) {
		if ((src != null && !"".equals(src.trim())) && (src.contains("http://") || src.contains("https://"))) {
			boobsList.add(src);
		} else {
			boobsList.add(BASE_URL + src);
		}
	}

	public List<FrideeDto> scrapePosts(String url) {
		try {
			final var document = Jsoup.connect(url).get();
			final var posts = new ArrayList<FrideeDto>();

			document.select("div.shout").forEach(element -> {
				String postId = element.attr("data-id");
				String title = element.select("div.shout_title a").text();
				posts.add(new FrideeDto(Integer.parseInt(postId), title, null, null, null));
			});

			return posts;
		} catch (IOException e) {
			log.error("Error scrape", e);
			throw new ScrapeException(e);
		}
	}

}
