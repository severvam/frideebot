package eu.alpinweiss.telegram.scraper;

public class ScrapeException extends RuntimeException {

	public ScrapeException(Throwable cause) {
		super(cause);
	}

}
