package eu.alpinweiss.telegram.scraper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
class VideoDto {

	private String provider;
	private String videoId;

	String fullUrl() {
		if ("vimeo".equals(provider)) {
			return "https://vimeo.com/" + videoId;
		}
		return "https://www.youtube.com/watch?v=" + videoId;
	}

}
