package eu.alpinweiss.telegram;

import eu.alpinweiss.telegram.config.ChatConfigService;
import eu.alpinweiss.telegram.config.ChatNotConfiguredException;
import eu.alpinweiss.telegram.config.DaggerAppComponent;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.LogManager;

@Slf4j
public class App {

	private static final int DEFAULT_PORT = 3100;

	public static void main(String[] args) throws IOException {
		setupLogger();

		final var httpServer = Vertx.vertx().createHttpServer();
		final var router = Router.router(Vertx.vertx());

		final var appComponent = DaggerAppComponent.create();
		final var chatPollingService = new ChatPollingService(appComponent.getCommandExecutor(), appComponent.getTelegramBotApi());
		final var routeInitializer = new RouteInitializer();

		initChat(appComponent.getChatConfigService());
		chatPollingService.startPolling();

		routeInitializer.initRoutes(router, appComponent.getRouteHandlers());
		httpServer.requestHandler(router::accept).listen(DEFAULT_PORT, routeInitializer.listenHandler(DEFAULT_PORT));
	}

	private static void initChat(ChatConfigService chatConfigService) {
		try {
			chatConfigService.chatId();
		} catch (ChatNotConfiguredException e) {
			log.error("Chat not configured", e);
			System.exit(1);
		}
	}

	private static void setupLogger() throws IOException {
		createLogsPath();
		final var stream = App.class.getResourceAsStream("/logging.properties");
		LogManager.getLogManager().readConfiguration(stream);
	}

	private static void createLogsPath() throws IOException {
		final var path = Paths.get("logs/");

		if (Files.notExists(path)) {
			log.info("Target dir 'logs/' will be created.");
			Files.createDirectory(path);
		}
	}

}
