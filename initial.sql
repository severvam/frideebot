CREATE DATABASE frideebot
  DEFAULT CHARACTER SET utf8mb4
  DEFAULT COLLATE utf8mb4_general_ci;

CREATE USER 'frideebot'@'%' IDENTIFIED BY 'secret';
GRANT ALL PRIVILEGES ON frideebot.* TO 'frideebot'@'%';

USE frideebot;

CREATE TABLE IF NOT EXISTS fridee (
  id            INT AUTO_INCREMENT primary key NOT NULL,
  title         VARCHAR(1000),
  url           VARCHAR(1000),
  createdDate   DATE,
  publishedDate DATE,
  enabled       BOOL
);


CREATE TABLE IF NOT EXISTS fridee_boobie (
  id        INT AUTO_INCREMENT primary key NOT NULL,
  fridee_id int,
  url       VARCHAR(1000),
  enabled   BOOL,
  posted    BOOL
);


CREATE TABLE IF NOT EXISTS fridee_video (
  id        INT AUTO_INCREMENT primary key NOT NULL,
  fridee_id int,
  url       VARCHAR(1000),
  enabled   BOOL,
  posted    BOOL
);